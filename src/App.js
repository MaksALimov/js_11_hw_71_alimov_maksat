import Header from "./components/Header/Header";
import Dishes from "./components/Dishes/Dishes";
import {Route} from "react-router";
import AddNewDish from "./containers/AddNewDish/AddNewDish";
import EditDishes from "./containers/EditDIshes/EditDishes";
import Orders from "./containers/Orders/Orders";
import UserOrder from "./components/UserOrder/UserOrder";

const App = () => {
    return (
        <>
            <Header/>
            <Route path="/" exact component={Dishes}/>
            <Route path="/add" component={AddNewDish}/>
            <Route path="/edit" component={EditDishes}/>
            <Route path="/orders" component={Orders}/>
            <Route path="/userOrder" component={UserOrder}/>
        </>
    )
};

export default App;
