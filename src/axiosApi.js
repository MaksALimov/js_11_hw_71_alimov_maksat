import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://order-pizza-js-11-default-rtdb.firebaseio.com',
});

export default axiosApi;