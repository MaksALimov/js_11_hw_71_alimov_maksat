import React, {useCallback, useEffect} from 'react';
import './Dishes.css';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteDishes, getDishes} from "../../store/actions/dishesActions";
import {getEditDish} from "../../store/actions/editDishesActions";

const Dishes = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);

    const deleteDish = useCallback(() => {
        dispatch(getDishes());
    }, [dispatch]);

    useEffect(() => {
        dispatch(getDishes());
    }, [dispatch, deleteDish]);

    return (
        <>
            <div className="Dishes">
                <h3 className="Dishes__title">Dishes</h3>
                <Link to="/add">
                    <button className="Dishes__add-btn">Add new Dish</button>
                </Link>
            </div>
            {Object.keys(dishes).map(dish => (
                <div key={dishes[dish].name} className="DishesContent">
                    <img className="DishesContent__img" src={dishes[dish].image} alt="dish"/>
                    <h4 className="DishesContent__name">{dishes[dish].name}</h4>
                    <p className="DishesContent__price">{dishes[dish].price} KGZ</p>
                    <Link to="/edit">
                        <button
                            className="DishesContent__edit-btn"
                            onClick={() => dispatch(getEditDish(dish))}
                        >
                            Edit
                        </button>
                    </Link>
                    <button
                        className="DishesContent__delete-btn"
                        onClick={() => dispatch(deleteDishes(dish, deleteDish))}>
                        Delete
                    </button>
                </div>
            ))}
        </>
    );
};

export default Dishes;