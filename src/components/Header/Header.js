import React from 'react';
import './Header.css';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <>
            <div className="Header">
                <Link to="/">
                    <h1 className="Header__main-title">Turtle Pizza Admin</h1>
                </Link>
                <Link to="/userOrder">
                    <h2 className="Header__user-order">Order pizza</h2>
                </Link>
                <div>
                    <Link to="/">
                        <h2 className="Header__dishes">Dishes</h2>
                    </Link>
                    <Link to="/orders">
                        <h2 className="Header__orders">Orders</h2>
                    </Link>
                </div>
            </div>
            <div className="Header__line"/>
        </>
    );
};

export default Header;