import React, {useEffect} from 'react';
import './UserOrder.css';
import {useDispatch, useSelector} from "react-redux";
import {getUserDishes} from "../../store/actions/userOrderActions";
import {getOrder} from "../../store/actions/ordersAction";

const UserOrder = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getUserDishes());
    }, [dispatch]);

    const userDishes = useSelector(state => state.userOrders.dishes);
    const orders = useSelector(state => state.orders.orders);

    return (
        <>
            {Object.keys(userDishes).map(dish => (
                <div key={userDishes[dish].name} className="UserOrderContainer">
                    <img src={userDishes[dish].image} alt="pizza"/>
                    <p>{userDishes[dish].name}</p>
                    <p>{userDishes[dish].price} KGZ</p>
                    <button onClick={() => dispatch(getOrder(userDishes[dish], orders, dish))}>Order</button>
                </div>
            ))}
        </>
    );
};

export default UserOrder;