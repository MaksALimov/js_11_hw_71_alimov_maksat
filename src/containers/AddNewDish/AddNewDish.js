import React, {useState} from 'react';
import './AddNewDish.css';
import {useDispatch} from "react-redux";
import {submitDish} from "../../store/actions/addNewDishActions";

const AddNewDish = () => {
    const dispatch = useDispatch();
    const [order, setOrder] = useState({
        name: '',
        price: '',
        image: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setOrder(prevState => ({...prevState, [name] : value}));
    };

    const addNewDish = e => {
        e.preventDefault();
        dispatch(submitDish(order));
    };


    return (
        <>
            <form className="AddNewDishWrapper" onSubmit={addNewDish}>
                <input
                    type="text"
                    name="name"
                    placeholder="Название блюдо"
                    onChange={onInputChange}
                />
                <input
                    type="text"
                    name="price"
                    placeholder="Укажите цену"
                    onChange={onInputChange}
                />
                <input
                    type="text"
                    name="image"
                    placeholder="Ссылка на картинку"
                    onChange={onInputChange}
                />
                <button type="submit" className="AddNewDishWrapper__submit-btn">Submit</button>
            </form>
        </>
    );
};

export default AddNewDish;