import React from 'react';
import './EditDishes.css';
import {useDispatch, useSelector} from "react-redux";
import {
    editDish,
    onEditInputImageChange,
    onEditInputNameChange,
    onEditInputPriceChange
} from "../../store/actions/editDishesActions";

const EditDishes = () => {
    const dispatch = useDispatch();
    const editDishName = useSelector(state => state.editDishes.name);
    const editDishPrice = useSelector(state => state.editDishes.price);
    const editDishImage = useSelector(state => state.editDishes.image);
    const editDishId = useSelector(state => state.editDishes.editDishId);

    const onInputNameChange = inputNameValue => {
        dispatch(onEditInputNameChange(inputNameValue));
    };

    const onInputPriceChange = inputPriceValue => {
        dispatch(onEditInputPriceChange(inputPriceValue));
    };

    const onInputImageChange = inputImageValue => {
        dispatch(onEditInputImageChange(inputImageValue));
    };

    const editDishes = e => {
        e.preventDefault();
        dispatch(editDish(editDishId, editDishName, editDishPrice, editDishImage));
    };

    return (
        <>
            <h3 className="EditDishesTitle">Странца редактирования блюд</h3>
            <form className="EditDishesWrapper" onSubmit={editDishes}>
                <input
                    type="text"
                    name="name"
                    placeholder="Название блюдо"
                    value={editDishName}
                    onChange={e => onInputNameChange(e.target.value)}
                />
                <input
                    type="text"
                    name="price"
                    placeholder="Укажите цену"
                    value={editDishPrice}
                    onChange={e => onInputPriceChange(e.target.value)}
                />
                <input
                    type="text"
                    name="image"
                    placeholder="Ссылка на картинку"
                    value={editDishImage}
                    onChange={e => onInputImageChange(e.target.value)}
                />
                <button type="submit" className="EditDishesWrapper__submit-btn">Submit</button>
            </form>
        </>
    );
};

export default EditDishes;