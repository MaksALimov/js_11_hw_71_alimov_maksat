import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Orders.css';
import {deleteOrder} from "../../store/actions/ordersAction";

const Orders = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);
    const delivery = useSelector(state => state.orders.delivery);
    const currentId = useSelector(state => state.orders.currentId);

    return (
        <div>
            {orders.map((dish, index) => (
                <div key={dish.name} className="OrdersWrapper">
                    <h3>x{dish.amount} {dish.name}</h3>
                    <p className="OrdersWrapper__price">{dish.price} KGZ</p>
                    <p className="OrdersWrapperCost__title">
                        Order Total {dish.price * dish.amount + delivery * dish.amount}
                    </p>
                    <div className="OrdersWrapperCost">
                        <p>Delivery {delivery}</p>
                        <button className="OrdersWrapper__complete-order" onClick={() => dispatch(deleteOrder(currentId, dish.name))}>Complete Order</button>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default Orders;