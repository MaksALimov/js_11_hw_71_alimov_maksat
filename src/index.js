import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore, applyMiddleware, compose, combineReducers} from "redux";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import addNewDishReducer from "./store/reducers/addNewDishReducer";
import dishesReducer from "./store/reducers/DishesReducer";
import editDishesReducer from "./store/reducers/EditDishesReducer";
import orderReducer from "./store/reducers/ordersReducer";
import userOrdersReducer from "./store/reducers/userOrdersReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    addDish: addNewDishReducer,
    dishes: dishesReducer,
    editDishes: editDishesReducer,
    orders: orderReducer,
    userOrders: userOrdersReducer,
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>
)

ReactDOM.render(app, document.getElementById('root'));
