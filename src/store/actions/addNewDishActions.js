import axiosApi from "../../axiosApi";

export const NEW_DISH_REQUEST = 'NEW_DISH_REQUEST';
export const NEW_DISH_SUCCESS = 'NEW_DISH_SUCCESS';
export const NEW_DISH_FAILURE = 'NEW_DISH_FAILURE';

export const newDishRequest = () => ({type: NEW_DISH_REQUEST});
export const newDishSuccess = () => ({type: NEW_DISH_SUCCESS});
export const newDishFailure = error => ({type: NEW_DISH_FAILURE, payload: error});

export const submitDish = order => {
    return async dispatch => {
        try {
            dispatch(newDishRequest());

            await axiosApi.post('/dishes.json', order);
            dispatch(newDishSuccess());
        } catch (error) {
            dispatch(newDishFailure(error));
        }
    };
};