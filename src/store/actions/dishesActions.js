import axiosApi from "../../axiosApi";

export const DISHES_REQUEST = 'DISHES_REQUEST';
export const DISHES_SUCCESS = 'DISHES_SUCCESS';
export const DISHES_FAILURE = 'DISHES_FAILURE';

export const DELETE_DISHES_REQUEST = 'DELETE_DISHES';
export const DELETE_DISHES_SUCCESS = 'DELETE_DISHES_SUCCESS';
export const DELETE_DISHES_FAILURE = 'DELETE_DISHES_FAILURE';

export const dishesRequest = () => ({type: DISHES_REQUEST});
export const dishesSuccess = dishes => ({type: DISHES_SUCCESS, payload: dishes});
export const dishesFailure = error => ({type: DISHES_FAILURE, payload: error});
export const deleteDishesRequest = () => ({type: DELETE_DISHES_REQUEST});
export const deleteDishesFailure = error => ({type: DELETE_DISHES_FAILURE, payload: error});
export const deleteDishesSuccess = () => ({type: DELETE_DISHES_SUCCESS});

export const getDishes = () => {
    return async dispatch => {
        try {
            dispatch(dishesRequest());

            const response = await axiosApi.get('/dishes.json');
            dispatch(dishesSuccess(response.data));
        } catch (error) {
            dispatch(dishesFailure(error));
        }
    };
};

export const deleteDishes = (orderId, deleteDish) => {
    return async dispatch => {
        try {
            dispatch(deleteDishesRequest());

            await axiosApi.delete(`/dishes/${orderId}.json`);
            dispatch(deleteDishesSuccess());
            deleteDish();
        } catch (error) {
            dispatch(deleteDishesFailure(error));
        }
    }
};