import axiosApi from "../../axiosApi";

export const GET_EDIT_DISH_ID = 'GET_EDIT_DISH_ID';

export const ON_EDIT_INPUT_NAME_CHANGE = 'ON_EDIT_INPUT_NAME_CHANGE';
export const ON_EDIT_INPUT_PRICE_CHANGE = 'ON_EDIT_INPUT_PRICE_CHANGE';
export const ON_EDIT_INPUT_IMAGE_CHANGE = 'ON_EDIT_INPUT_IMAGE_CHANGE';

export const GET_EDIT_DISH_REQUEST = 'GET_EDIT_DISH_REQUEST';
export const GET_EDIT_DISH_SUCCESS = 'GET_EDIT_DISH_SUCCESS';
export const GET_EDIT_DISH_FAILURE = 'GET_EDIT_DISH_FAILURE';

export const EDIT_DISH_REQUEST = 'EDIT_DISH_REQUEST';
export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';
export const EDIT_DISH_FAILURE = 'EDIT_DISH_FAILURE';

export const getEditDishId = editDishId => ({type: GET_EDIT_DISH_ID, payload: editDishId});

export const getEditDishRequest = () => ({type: GET_EDIT_DISH_REQUEST});
export const getEditDishSuccess = dishData => ({type: GET_EDIT_DISH_SUCCESS, payload: dishData});
export const getEditDishFailure = error => ({type: GET_EDIT_DISH_FAILURE, payload: error});

export const onEditInputNameChange = inputNameValue => ({type: ON_EDIT_INPUT_NAME_CHANGE, payload: inputNameValue});
export const onEditInputPriceChange = inputPriceValue => ({type: ON_EDIT_INPUT_PRICE_CHANGE, payload: inputPriceValue});
export const onEditInputImageChange = inputImageValue => ({type: ON_EDIT_INPUT_IMAGE_CHANGE, payload: inputImageValue});

export const editDishRequest = () => ({type: EDIT_DISH_REQUEST});
export const editDishSuccess = () => ({type: EDIT_DISH_SUCCESS});
export const editDishFailure = error => ({type: EDIT_DISH_FAILURE, payload: error});

export const getEditDish = editOrderId => {
    return async dispatch => {
        try {
            dispatch(getEditDishRequest());

            const response = await axiosApi.get(`/dishes/${editOrderId}.json`);
            dispatch(getEditDishSuccess(response.data));
            dispatch(getEditDishId(editOrderId));
        } catch (error) {
            dispatch(getEditDishFailure(error));
        }
    };
};

export const editDish = (editDishId, name, price, image) => {
    return async dispatch => {
        try {
            dispatch(editDishRequest());

            await axiosApi.put(`/dishes/${editDishId}.json`, {name, price, image});

            dispatch(editDishSuccess());
        } catch (error) {
            dispatch(editDishFailure());
        }
    };
};