import axiosApi from "../../axiosApi";

export const ADD_ORDERS = 'ADD_ORDERS';
export const DELETE_ORDER_REQUEST = 'DELETE_ORDER_REQUEST';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE';
export const CURRENT_ID = 'CURRENT_ID';

export const addOrder = order => ({type: ADD_ORDERS, payload: order});
export const deleteOrderRequest = () => ({type: DELETE_ORDER_REQUEST});
export const deleteOrderSuccess = orderName => ({type: DELETE_ORDER_SUCCESS, payload: orderName});
export const deleteOrderFailure = error => ({type: DELETE_ORDER_FAILURE, payload: error});
export const currentId = id => ({type: CURRENT_ID, payload: id});

export const getOrder = (order, ordersArray, orderId) => {
    return async dispatch => {
        const ordersArrayCopy = [...ordersArray];
        const index = ordersArrayCopy.findIndex(id => id.name === order.name);
        dispatch(currentId(orderId));

        if (index === -1) {
            ordersArrayCopy.push({name: order.name, price: order.price, amount: 1});
        } else {
            ordersArrayCopy[index].amount++;
        }
        try {
            if (ordersArrayCopy.length === 1) {
                await axiosApi.post('/orders.json', {[orderId]: ordersArrayCopy[0].amount});
            } else {
                await axiosApi.post('/orders.json', {[orderId]: ordersArrayCopy[ordersArrayCopy.findIndex(id => id.name === order.name)].amount});
            }

        } catch (error) {
            throw Error;
        }
        dispatch(addOrder(ordersArrayCopy));

        dispatch(addOrder(ordersArrayCopy));
    };
};

export const deleteOrder = (orderId, orderName) => {
    return async dispatch => {
        try {
            dispatch(deleteOrderRequest());

            await axiosApi.delete(`/dishes/${orderId}.json`);
            dispatch(deleteOrderSuccess(orderName))
        } catch (error) {
            dispatch(deleteOrderFailure(error));
        }
    };
};