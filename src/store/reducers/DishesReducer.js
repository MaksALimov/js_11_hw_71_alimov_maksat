import {
    DELETE_DISHES_FAILURE,
    DELETE_DISHES_REQUEST,
    DELETE_DISHES_SUCCESS,
    DISHES_FAILURE,
    DISHES_REQUEST,
    DISHES_SUCCESS
} from "../actions/dishesActions";

const initialState = {
    dishes: [],
    error: null,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISHES_REQUEST: {
            return {...state};
        }

        case DISHES_SUCCESS: {
            return {...state, dishes: action.payload, error: false};
        }

        case DISHES_FAILURE: {
            return {...state, error: action.payload};
        }

        case DELETE_DISHES_REQUEST: {
            return {...state};
        }

        case DELETE_DISHES_SUCCESS: {
            return {...state, error: false};
        }

        case DELETE_DISHES_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default dishesReducer;