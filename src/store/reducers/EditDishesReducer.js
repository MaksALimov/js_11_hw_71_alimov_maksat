import {
    EDIT_DISH_FAILURE,
    EDIT_DISH_REQUEST, EDIT_DISH_SUCCESS,
    GET_EDIT_DISH_FAILURE, GET_EDIT_DISH_ID,
    GET_EDIT_DISH_REQUEST,
    GET_EDIT_DISH_SUCCESS, ON_EDIT_INPUT_IMAGE_CHANGE, ON_EDIT_INPUT_NAME_CHANGE, ON_EDIT_INPUT_PRICE_CHANGE,
} from "../actions/editDishesActions";

const initialState = {
    error: null,
    name: '',
    price: '',
    image: '',
    editDishId: null,
};

const editDishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_EDIT_DISH_REQUEST: {
            return {...state};
        }

        case GET_EDIT_DISH_SUCCESS: {
            return {
                ...state,
                name: action.payload.name,
                price: action.payload.price,
                image: action.payload.image,
                error: false
            };
        }

        case GET_EDIT_DISH_FAILURE: {
            return {...state, error: action.payload}
        }

        case ON_EDIT_INPUT_NAME_CHANGE: {
            return {...state, name: action.payload};
        }

        case ON_EDIT_INPUT_PRICE_CHANGE: {
            return {...state, price: action.payload};
        }

        case ON_EDIT_INPUT_IMAGE_CHANGE: {
            return {...state, image: action.payload};
        }

        case GET_EDIT_DISH_ID: {
            return {...state, editDishId: action.payload};
        }

        case EDIT_DISH_REQUEST: {
            return {...state};
        }

        case EDIT_DISH_SUCCESS: {
            return {...state, error: false};
        }

        case EDIT_DISH_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default editDishesReducer;