import {NEW_DISH_FAILURE, NEW_DISH_REQUEST, NEW_DISH_SUCCESS} from "../actions/addNewDishActions";

const initialState = {
    error: null,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case NEW_DISH_REQUEST: {
            return {...state};
        }

        case NEW_DISH_SUCCESS: {
            return {...state, error: false};
        }

        case NEW_DISH_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default cartReducer;