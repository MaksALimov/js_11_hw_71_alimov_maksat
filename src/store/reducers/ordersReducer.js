import {
    ADD_ORDERS, CURRENT_ID,
    DELETE_ORDER_FAILURE,
    DELETE_ORDER_REQUEST,
    DELETE_ORDER_SUCCESS,
} from "../actions/ordersAction";

const initialState = {
    orders: [],
    delivery: 150,
    error: null,
    currentId: null,
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ORDERS: {
            return {...state, orders: action.payload};
        }

        case DELETE_ORDER_REQUEST: {
            return {...state};
        }

        case DELETE_ORDER_SUCCESS: {
            return {...state, error: false, orders: state.orders.filter(dish => dish.name !== action.payload)};
        }

        case DELETE_ORDER_FAILURE: {
            return {...state, error: action.payload};
        }

        case CURRENT_ID: {
            return {...state, currentId: action.payload}
        }

        default:
            return state;
    }
};

export default orderReducer;