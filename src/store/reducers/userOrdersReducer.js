import {GET_DISHES_FAILURE, GET_DISHES_REQUEST, GET_DISHES_SUCCESS} from "../actions/userOrderActions";

const initialState = {
    dishes: [],
    error: null,
};

const userOrdersReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DISHES_REQUEST: {
            return {...state}
        }

        case GET_DISHES_SUCCESS: {
            return {...state, dishes: action.payload, error: false};
        }

        case GET_DISHES_FAILURE: {
            return {...state, error: action.payload};
        }


        default:
            return state;
    }
};

export default userOrdersReducer;